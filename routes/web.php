<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*PREFIXO DE ROTAS DE TESTE para o app guzzle*/
Route::group(['prefix' => 'test'], function () {
    Route::post('/guzzle', 'GuzzleController@getMinWageTable')->name('guzzle-get');
});

