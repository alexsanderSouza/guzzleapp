<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class GuzzleController extends Controller
{
    /**
     * Ontém dados do salário minímo com o guzzle
     *
     * @return array
     */
    public function getMinWageTable()
    {
        $array = [];
        $client = new Client(); //GuzzleHttp\Client
        $result =  $client->request('GET', 'http://www.guiatrabalhista.com.br/guia/salario_minimo.htm');
        $body = (string) $result->getBody(); //obtém corpo da página html

        /* instância do crawler */
        $crawler = new Crawler($body);

        /* obtendo dados da tabela com DOMCrawler */
        $table = $crawler->filter('table > tbody > tr')->each(function ($tr, $i) {
            /* percorre as colunas da linha obtendo os valores */
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

        /* ordenando array para resposta conforme pedido no teste */
        foreach ($table as $key1 => $line) {
            foreach ($line as $key2 => $column) {
                if($key1 != 0){
                    $array[($key1 - 1)][$table[0][$key2]] = $column;
                }
            }
        }
        dd($array); //opção de debug para melhor visulização do array
        // print_r($array); imprimi array na tela
    }
}
